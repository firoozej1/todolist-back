import { Router, Request, Response } from 'express';
import httpStatus from 'http-status';
import { TodoService } from 'services/TodoService';

export const registerRoutes = (router: Router) => {
    const todoService = new TodoService();
    router.get('/getTodoName', (req: Request, res: Response) => {
        const title = todoService.generateTodoTitle();
        res.status(httpStatus.OK).send(title);
    });
    router.get('/getTodos', (req: Request, res: Response) => {
        const tasks = todoService.getAllTodos();
        res.status(httpStatus.OK).send(tasks);
    });
    router.post('/addTodo', (req: Request, res: Response) => {
        const task = todoService.addTodo(req.body);
        res.status(httpStatus.OK).send(task);
    });
    router.delete('/removeTodos', (req: Request, res: Response) => {
        const ids = todoService.removeTodos(req.body);
        res.status(httpStatus.OK).send(ids);
    });
    router.patch('/toggleTodosStatus', (req: Request, res: Response) => {
        const tasks = todoService.toggleTodosStaus(req.body);
        res.status(httpStatus.OK).send(tasks);
    });
};
