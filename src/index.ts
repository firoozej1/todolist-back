import express, { Router, Express } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { registerRoutes } from './routes';

class Server {
    private express: Express;
    readonly port: string;

    constructor(port = '4000') {
        this.port = port;
        this.express = express();
        this.express.use(bodyParser.json());
        this.express.use(cors());
        const router = Router();
        this.express.use(router);
        registerRoutes(router);
        this.express.listen(this.port, () => {
            console.log(`The todo application is running on http://localhost:${this.port}`);
        });
    }
}

try {
    new Server();
} catch (e) {
    console.log(e);
    process.exit(1);
}

process.on('uncaughtException', (err) => {
    console.log('uncaughtException', err);
    process.exit(1);
});
