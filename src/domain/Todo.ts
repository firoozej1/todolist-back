import { TodoStatus } from './TodoStatus';

export class Todo {
    private id: string;
    private title: string;
    private status: TodoStatus;
    constructor(id: string, title: string, status: TodoStatus) {
        this.id = id;
        this.title = title;
        this.status = status;
    }
    getId() {
        return this.id;
    }
    getTitle() {
        return this.title;
    }
    toggleStatus() {
        this.status = this.status === TodoStatus.DONE ? TodoStatus.UNDONE : TodoStatus.DONE;
    }
}
