export enum TodoStatus {
    DONE = 1,
    UNDONE = 0,
}
