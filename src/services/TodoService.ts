var randomstring = require('randomstring');
import { v4 as uuidv4 } from 'uuid';
import { Todo } from 'domain/Todo';
import { TodoDto } from 'application/TodoDto';
import { TodoStatus } from 'domain/TodoStatus';

export class TodoService {
    private todos: Todo[];
    constructor() {
        this.todos = [new Todo(uuidv4(), 'Create todo list', TodoStatus.DONE)];
    }
    getAllTodos() {
        return this.todos;
    }
    generateTodoTitle() {
        return randomstring.generate({
            length: 12,
            charset: 'alphabetic',
        });
    }
    addTodo(newTodo: TodoDto) {
        const todo = new Todo(uuidv4(), newTodo.title, newTodo.status);
        this.todos = [...this.todos, todo];
        return todo;
    }
    toggleTodosStaus(ids: string) {
        let updatedTodos = [];
        for (let todo of this.todos) {
            if (ids.includes(todo.getId())) {
                todo.toggleStatus();
                updatedTodos.push(todo);
            }
        }
        return updatedTodos;
    }
    removeTodos(ids: string[]) {
        this.todos = this.todos.filter((todo) => !ids.includes(todo.getId()));
        return ids;
    }
}
