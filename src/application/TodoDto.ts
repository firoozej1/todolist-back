import { TodoStatus } from "domain/TodoStatus";

export class TodoDto {
    public id: string;
    public title: string;
    public status: TodoStatus;
    constructor(id: string, title: string, status: TodoStatus) {
        this.id = id;
        this.title = title;
        this.status = status;
    }
}
